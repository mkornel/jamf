# Use provided mockups to recreate a wizard.
# To solve the task use included technology stack.

# All mockups and assets can be found here:
https://www.dropbox.com/sh/36hwzr44x4lths9/AACDZ-H-JFXopzRDIIZ0s0n_a?dl=0
# Font: ProximaNova

# Task Comments:
#   User is not able to proceed to the next step unless he has filled all required info.
#   Use 'mock.json' data to fill the select options.
#   Clicking 'Upload Avatar' will open a modal and create an overlay layer.
#   User should be able to verify all the info in the last step, before submitting it.
#   No need for any submit action.
#   Avatar preview WILL be treated as a bonus task.
#   Drag&Drop functionality is NOT required and will NOT be treated as a bonus task.

# Bonus points will be given for:
#   Reusability
#   Unit & Integration tests coverage
#   Attention to detail
#   Error handling

# Let us know if you liked the task or what can we improve in Comments.md file