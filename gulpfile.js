var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var ts = require("gulp-typescript");
var gls = require('gulp-live-server');
var clean = require('gulp-clean');

gulp.task('css', function () {
    gulp.src(['./src/**/*.scss'], {base: './src/'})
        .pipe($.sass({ style: 'expanded' }))
        .pipe(gulp.dest('./build/'));
});


gulp.task('ts', function () {
    return gulp.src('src/**/*.ts', {base: './src/'})
        .pipe(ts({
            target: 'es5'
        }))
        .js
        .pipe(gulp.dest('build'));
});


gulp.task('html', function () {
    return gulp.src('./src/**/*.html', {base: './src/'})
        .pipe(gulp.dest('./build'));
});


gulp.task('watch', function () {
    gulp.watch(['./src/**/*.scss'], ['css']);
    gulp.watch(['./src/**/*.ts'], ['ts']);
    gulp.watch(['./src/**/*.html'], ['html']);
});



gulp.task('default', ['css', 'ts', 'html'], function() {
    var server = gls.new('./server.js');
    return server.start();
});

gulp.task('dev', ['css', 'ts', 'html', 'watch'], function() {
    var server = gls.new('./server.js');
    return server.start();
});
