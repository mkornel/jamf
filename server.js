const express = require('express'),
    http = require('http'),
    path = require('path'),
    bodyParser = require('body-parser'),
    multer = require('multer');


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
      cb(null, "avatar_" + req.query.name + '.png');
    }
})

var upload = multer({ storage: storage }).single('file');

var app = express();

app.set('port', 3000);

app.use(express.static(path.normalize(__dirname + '/')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/" + "index.html");
});

app.post('/user/registrate', function (req, res) {
    res.send({ success: true });
});

app.post('/upload', function (req, res, next) {
    upload(req, res, function (err) {
       if (err) {
            console.log(err);
            res.send({ success: false });
       }
       else{
            res.send({ success: true });
       }
       
 });     
})

http.createServer(app).listen(app.get('port'), function () {
    console.log('Server listening on port ' + app.get('port'));
});