// load our dependencies
head.load(
    'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.13.1/lodash.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.5/angular.min.js',
    'http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-route.js',
    'https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.min.js',
    
    'build/app/app.js',
    'build/app/app.css',
    'build/app/models/response.model.js',
    'build/app/models/user.model.js',
    'build/app/interfaces/auth.interface.js',
    'build/app/interfaces/regController.interface.js',
    'build/app/directives/fileUpload/fileUpload.directive.js',
    'build/app/directives/fileUpload/fileUpload.directive.css',
    'build/app/services/auth.service.js',
    'build/app/services/modal.service.js',
    'build/app/modules/registration/registration.config.js',
    'build/app/modules/registration/registration.component.js',
    'build/app/modules/registration/registration.css'
);