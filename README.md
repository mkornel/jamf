# Intsall the project:

Open a CMD in the foder and run command:

```
npm install
```

# Start the project:

When all node modules are installed, in the CMD run command:

```
npm start
```

# View the project:

When you see in the CMD the comment "Server listening on port 3000", open a browser and enter URL localhost:3000.