angular.module('registration', ['ngRoute'])
    .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

        var templatesPath = '/build/app/modules/registration/templates/';

        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/login-details', {
                templateUrl: templatesPath + 'loginDetails.html',
            })
            .when('/user-group', {
                templateUrl: templatesPath + 'userGroup.html',
            })
            .when('/avatar', {
                templateUrl: templatesPath + 'avatar.html',
            })
            .when('/submit-user', {
                templateUrl: templatesPath + 'submitUser.html',
            })
            .otherwise({ redirectTo: '/login-details' });
    }]);