var angular = angular;
declare var Promise: any;

import {User} from "../../models/user.model";
import {RegControllerInterface} from "../../interfaces/regController.interface";
import {ModalInterface} from "../../interfaces/modal.interface";
import {AuthInterface} from "../../interfaces/auth.interface";
import {Response} from "../../models/response.model";

class RegController implements RegControllerInterface{

    user:User.Registration = {
        email: null,
        password: null,
        userGroup: null,
        avatar: null
    };

    verPassword:string = null;
    userGroups:Array<Object>;

    selectedMenu:number = 1;

    urls:Object = {
        1: "/login-details",
        2: "/user-group",
        3: "/avatar",
        4: "/submit-user"
    };

    static $inject = ['$scope', '$http', '$rootScope', '$location', '$log', '$auth', '$modal'];

    constructor(private $scope, private $http, private $rootScope, private $location, private $log, private $auth:AuthInterface, private $modal:ModalInterface) {

        this.getUserGroups().then((data:Array<Object>)=>{
            this.userGroups = data;
        })

        $rootScope.$on("$locationChangeStart", (event, next, current)=>{
            if(!this.user.email || !this.user.password){
                this.$location.path(this.urls[1]);
            }
        });

    }

    getUserGroups(){
        return new Promise((resolve, reject)=>{
            this.$http.get('mock.json').success((response:Array<Object>) => {
                resolve(response);
            }).error((error)=>{
                reject(error);
            });
        })
    }


    cancel() {
        switch(this.selectedMenu){
            case 1:
                this.user.email = null;
                this.user.password = null;
                this.verPassword = null;
                break;
            case 2:
                this.user.userGroup = null;
                this.selectedMenu = 1;
                break;
            case 3:
                this.user.avatar = null;
                this.selectedMenu = 2;
                break;
            case 4:
                this.selectedMenu = 3;
                break;
            default:
        }

        this.$location.path(this.urls[this.selectedMenu]);
    }

    next(frm) {

        if(!frm.$valid){
            return false;
        }

        if(frm.password){
            if(frm.password.$$rawModelValue != frm.verPassword.$$rawModelValue){
                return false;
            }
        }

        this.selectedMenu = this.selectedMenu + 1;
        this.$location.path(this.urls[this.selectedMenu]);
    }

    submit(){

        this.$auth.register(this.user, (success:Response.Success)=>{

            if(success){
                this.selectedMenu = 1;

                this.user.email = null;
                this.user.password = null;
                this.verPassword = null;
                this.user.userGroup = null;
                this.user.avatar = null;

                this.$modal.create({
                    type: "OK",
                    title: "Info",
                    width: "300px",
                    height: "200px",
                    message: "The user has been submitted. You will be navigated to the login details page!",
                    onOk: ()=>{
                        this.$location.path(this.urls[this.selectedMenu]);
                    }
                }, (modal)=>{
                    this.$modal.show(modal);
                });
            }
            else{
                this.$modal.create({
                    type: "OK",
                    title: "Error",
                    message: "The user has not been submitted.",
                    onOk: ()=>{
                        this.$location.path(this.urls[this.selectedMenu]);
                    }
                }, (modal)=>{
                    this.$modal.show(modal);
                });
            }
        });

    }

    popupAvatar(){
        this.$modal.create({
            type: "UPLOAD",
            title: "Avatar",
            width: "500px",
            height: "400px",
            message: "User Avatar. It is recommended that you use a file with the GIF or PNG format. The recommended size is 512x512 pixels.",
            uploadName: this.user.email.split("@")[0].replace(/[^a-zA-Z ]/g, ""),
            onUpload: (src)=>{
                this.$scope.$apply(()=>{
                    this.user.avatar = src;
                });
            }
        }, (modal)=>{
            this.$modal.show(modal);
        });
    }

}

export class RegComponent implements ng.IComponentOptions {
 
    public bindings:any;
    public controller:any;
    public templateUrl:string;

    constructor() {
        this.controller = RegController;
        this.templateUrl = '/build/app/modules/registration/registration.html';
    }

}

class Directive {
    private templateUrl:string;
    constructor(url) {
        this.templateUrl = url;
    }
    create(){
        return ()=>{
            return {
                templateUrl: this.templateUrl
            }
        }
    }
}

angular.module('registration').component('registration', new RegComponent());
angular.module('registration').directive('prevNextButtons', new Directive('/build/app/modules/registration/directives/prevNextButtons.html').create());
angular.module('registration').directive('navigation', new Directive('/build/app/modules/registration/directives/navigation.html').create());