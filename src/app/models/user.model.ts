export namespace User {

    export interface Registration {
        email:string;
        password:string;
        userGroup:number;
        avatar:string;
    }

    export interface Login {
        email:string;
        password:string;
    }

    export interface Logout {
        email:string;
    }

    export interface ChangePassword {
        email:string;
        newPassword:string;
        oldPassword:string;
    }

}