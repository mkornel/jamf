export namespace Modal {

    export interface Config {
        type?:string,
        title?:string,
        width?:string,
        height?:string,
        message?:string,
        onCancel?:Function
        onOk?:Function
        onUpload?:Function,
        uploadName?:string
    }

}