class FileUploadDirective {
    private templateUrl:string = "/build/app/directives/fileUpload/fileUpload.directive.html";
    create(){
        return ()=>{
            return {
                templateUrl: this.templateUrl,
                link: function (scope, element, attrs) {
                    
                    var fileInput = element.find('input')[0];

                    scope.internalControl = scope.control || {};
                    scope.internalControl.uploadFileName = "No file chosen";
                    //scope.internalControl.loading = false;
                    scope.file = null;

                    fileInput.onchange = function(event) {
                        //scope.internalControl.loading = false;
                        scope.$apply(function() {
                            var files = fileInput.files;
                            if (files[0]) {
                                scope.file = files[0];
                                scope.internalControl.uploadFileName = files[0].name;
                            }
                        });
                    }

                    scope.internalControl.chooseFile = function() {
                        fileInput.click();
                        //scope.internalControl.loading = true;
                    }

                }
            }
        }
    }
}

angular.module('jamf').directive('fileUpload', new FileUploadDirective().create());