var angular = angular;


import {User} from "../models/user.model";
import {Response} from "../models/response.model";
import {AuthInterface} from "../interfaces/auth.interface";

class AuthService implements AuthInterface {

    static $inject = ['$http', '$log'];

    constructor(private $http, private $log) {

    }

    register(user:User.Registration, callb:Function) {
        this.$http.post('/user/registrate').success((response:Response.Success) => {
            callb(response);
        }).error((error)=>{
            callb({success:false});
            this.$log.debug(error);
        });
    }

    login(user:User.Login, callb:Function) {
        this.$http.post('/user/login').success((response:Response.Success) => {
            callb(response);
        }).error((error)=>{
            callb({success:false});
            this.$log.debug(error);
        });
    }
    

    logout(user:User.Logout, callb:Function) {
        this.$http.get('/user/logout').success((response:Response.Success) => {
            callb(response);
        }).error((error)=>{
            callb({success:false});
            this.$log.debug(error);
        });
    }

    changePassword(user:User.ChangePassword, callb:Function) {
        this.$http.get('/user/changePassword').success((response:Response.Success) => {
            callb(response);
        }).error((error)=>{
            callb({success:false});
            this.$log.debug(error);
        });
    }
}

angular.module('jamf').service('$auth', AuthService)