var angular = angular;

import { Response } from "../models/response.model";
import { Modal } from "../models/modal.model";
import { ModalInterface } from "../interfaces/modal.interface";

class ModalService implements ModalInterface {

    static $inject = ['$compile', '$rootScope', '$http'];

    constructor(private $compile, private $rootScope, private $http) {

    }

    controller = {
        $http: null,
        title: null,
        message: null,
        close() {
            var modalContener = angular.element(document.querySelector('#modal_contener'));
            modalContener.remove();
        },
        ok: function () {
            this.close();
            if (this.onOk) {
                this.onOk();
            }
        },
        cancel: function () {
            this.close();
            if (this.onCancel) {
                this.onCancel();
            }
        },
        upload: function () {
            if(this.file){
                var file = this.file;
                var reader = new FileReader();
                reader.onload = this.imageIsLoaded;
                reader.onloadend = (e) => {
                    this.close();
                    if (this.onUpload) {
                        this.onUpload(e.target["result"]);
                    }
                }
                reader.readAsDataURL(file);

                var fd = new FormData();
                fd.append('file', file);

                this.$http.post("/upload?name=" + this["uploadName"] + "", fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).success((success: Response.Success) => {
                    console.log("uploaded!!");
                }).error(() => {
                    console.log("error!!");
                });
            }
        }
    };

    config: Modal.Config = {
        type: 'OK',
        title: 'Info',
        width: '400px',
        height: '300px'
    };

    create(config: Modal.Config, callb: Function) {
        this.config = angular.merge(this.config, config);
        this.countMargins();
        var modal = angular.element('<div id="modal_contener"></div>');
        var html = this.setUpHtml();
        var scope = this.setScopeForModal();
        this.$compile(html)(scope, (content) => {
            var modalWindow = angular.element(content);
            modalWindow.css(this.config);
            modal.append(modalWindow);
            callb(modal);
        });
    }

    setUpHtml() {

        var headerHtml = '<div class="header">' +
            '<div class="outer">' +
            '<div class="middle">' +
            '<div class="inner title">' +
            '{{title}}' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';


        var bodyHtml = '<div class="body">{{message}}</div>';


        var buttonsOKHtml = '<div class="buttons ok">' +
            '<button ng-click="ok()">OK</button>' +
            '</div>';


        var buttonsOKCANCELHtml = '<div class="buttons okcancel">' +
            '<button ng-click="cancel()">Cancel</button>' +
            '<button class="primary"ng-click="ok()">OK</button>' +
            '</div>';


        var buttonsUPLOADHtml = '<div class="buttons upload">' +
            '<button ng-click="cancel()">Cancel</button>' +
            '<button class="primary"ng-click="upload()">Upload</button>' +
            '</div>';


        var buttonsHtml = null;
        if (this.config.type == "OKCANCEL") {
            buttonsHtml = buttonsOKCANCELHtml;
        }
        else if (this.config.type == "UPLOAD") {
            buttonsHtml = buttonsUPLOADHtml;
            bodyHtml += '<file-upload></file-upload>';
        }
        else {
            buttonsHtml = buttonsOKHtml;
        }

        var modalHtml = '<div id="modal">' + headerHtml + bodyHtml + buttonsHtml + '</div>';

        return modalHtml;

    }

    setScopeForModal() {
        var scope = this.$rootScope.$new();
        if (this.config.onOk) {
            this.controller["onOk"] = this.config.onOk;
        }
        if (this.config.onCancel) {
            this.controller["onCancel"] = this.config.onCancel;
        }
        if (this.config.onUpload) {
            this.controller["onUpload"] = this.config.onUpload;
        }
        if (this.config.uploadName) {
            this.controller["uploadName"] = this.config.uploadName;
        }
        this.controller.message = this.config.message;
        this.controller.title = this.config.title;
        this.controller.$http = this.$http;

        angular.extend(scope, this.controller);

        return scope;
    }

    show(modal) {
        var body = angular.element(document).find('body');
        body.append(modal);
    }

    countMargins() {
        var width = parseInt(this.config.width.split("px")[0]);
        var height = parseInt(this.config.height.split("px")[0]);
        this.config["margin-left"] = "-" + width / 2 + "px";
        this.config["margin-top"] = "-" + height / 2 + "px";
    }

}

angular.module('jamf').service('$modal', ModalService)