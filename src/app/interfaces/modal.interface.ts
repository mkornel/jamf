import {Modal} from "../models/modal.model";

export interface ModalInterface {
    controller:Object;
    config:Object;
    create(config:Modal.Config, callb:Function);
    countMargins();
    show(modal);
    setScopeForModal();
}