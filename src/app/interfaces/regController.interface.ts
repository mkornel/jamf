import {User} from "../models/user.model";

export interface RegControllerInterface {
    user:User.Registration;
    verPassword:string;
    userGroups:Array<Object>;
    selectedMenu:number;
    urls:Object;

    cancel();
    next(frm);
    submit();
    popupAvatar();
}