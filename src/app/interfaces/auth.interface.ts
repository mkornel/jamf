import {User} from "../models/user.model";

export interface AuthInterface {
    register(user:User.Registration, callb:Function);
    login(user:User.Login, callb:Function);
    logout(user:User.Logout, callb:Function);
    changePassword(user:User.ChangePassword, callb:Function);
}